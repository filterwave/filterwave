import java.util.ArrayList;

public class Utility {

    public static float getMax(float a, float b){
        if(a>b){
            return a;
        }
        return b;
    }

    public static float getMin(float a, float b){
        if(a<b){
            return a;
        }
        return b;
    }

    public static float averageList(ArrayList<Float> list){
        int count = 0;
        float sum = 0;
        for (Float val : list) {
            count++;
            sum += val;
        };

        return sum/count;
    }

}
