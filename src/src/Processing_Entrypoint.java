import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import ddf.minim.analysis.FFT;
import processing.core.PApplet;
import processing.core.PImage;
//import com.hamoid.*;


import java.util.ArrayList;

public class Processing_Entrypoint extends PApplet {

    public static  String artistKeyword;
    public static PImage coverImage;


    String audioFileName = "../filterwave30secPreview.mp3"; // Audio file location

    float fps = 30;
    float smoothingFactor = 0.25f; // FFT audio analysis smoothing factor
    //
    // Global variables
    AudioPlayer track;
    FFT fft;
    Minim minim;




    // General
    int bands = 256; // must be multiple of two
    //float[] spectrum = new float[bands];
    float[] sum = new float[bands];


    public static void main(String[] args) {
        PApplet.main("Processing_Entrypoint");
        artistKeyword = "no keyword given";

    }

    /**
     * This is an example on how to pass variables to
     * @param artistKeyword
     */
    public static void startProcessing( String artistKeyword, PImage image) {
        coverImage = image;
        Processing_Entrypoint.artistKeyword = artistKeyword;
        PApplet.main("Processing_Entrypoint");
    }


    public void settings() {
        size(640, 640);
        smooth(8);
    }

    public void setup(){
        if(coverImage == null){
            String imagePath = "../testImage.jpg";
            coverImage =  loadImage(imagePath);
        }
        frameRate(fps);

        // Graphics related variable setting
        minim = new Minim(this);
        track = minim.loadFile(audioFileName, 2048);

        track.loop(0);




        fft = new FFT( track.bufferSize(), track.sampleRate() );

        fft.linAverages(bands);

        track.cue(60000); // Cue in milliseconds

        drawCoverImage();
        amplist = new ArrayList<Float>();
    }



    float max = 0;
    ArrayList<Float> amplist;
    int invertcounter = 0;

    public void draw() {
        drawTransparentCoverImage( 0);
        float[] spectrum = getSpectrum();

        //text(artistKeyword, 54,34);

        float bassAmp = getAverageAmp(spectrum, 0,15); //average: 11-15 Max:  22-30

        float highBassAmp = getAverageAmp(spectrum, 16, 70); //Max zwischen 1,6 und 5, Avg, knapp unter max
        //int highBassAmpp = (int) (highBassAmp * 50);

        float mediumHighAmp = getAverageAmp(spectrum, 71, 110);
        int mediumHighAmpp = (int) (mediumHighAmp * 50);

        float highAmp = getAverageAmp(spectrum, 111,190);
        int highAmpp = (int) (highAmp * 80);




        /** Bass Amp - Distortion HIGH
         * Display strong filter on strong bass
         * Spectrum: 0-20
         * */
        if(bassAmp>20 && highBassAmp <=4){
            //grayImage(8);
            drawTransparentCoverImage( 30);
        }

        if(bassAmp>18){
            if(invertcounter > 1){
                invertcounter--;
            }else{
                invertImage(11);
                invertcounter = 10;
            }
        }

        /** Medium Low Amp - Distortion MEDIUM
         * Display medium filter
         * Spectrum: 20-99
         * */
        if(highBassAmp>2){
            posterizeImage(mediumHighAmpp);
           //Pointillize(highAmpp,(int)highBassAmp*2);
        }

        //System.out.println("Medium Low Amp: "+ highBassAmpp);

        /** Medium High Amp - Distortion MEDIUM
         * Display medium filter
         * Spectrum: 150-199
         * */
        if(mediumHighAmpp>30){
            /* FILTER */
            //blurImage(mediumHighAmpp);
        }

        //System.out.println("Medium High Amp: "+ mediumHighAmpp);

        /** High Amp - Distortion MEDIUM
         * Spectrum: 200-254
         * */
        if(bassAmp > 5){
            waveyDistort(bassAmp*bassAmp/20,0.3f, 0.5f);

           // multiDilate(highAmpp /4, false);
        }
       //multiDilate((int)bassAmp/5, true);

        /** High Amp - Distortion HIGH
         * Spectrum: 200-254
         * */
        if(highAmpp>20){
            multiDilate(highAmpp/35, true);
            //System.out.println(highAmp);

        }

        //drawEqualizer(spectrum, true);

        //record("Filterwave_recording_noAudio",  30);

        printDebugInfo(mediumHighAmp);
    }

  /*  VideoExport videoExport;
    void record(String fileName, int frameRate){
        if(frameCount ==1){
            videoExport = new VideoExport(this, "" + fileName + ".mp4");
            videoExport.setFrameRate(frameRate);
            videoExport.startMovie();
        }
        videoExport.saveFrame();
    }*/

    public void printDebugInfo(float relevantAmp){
        max = Utility.getMax(relevantAmp, max);
        System.out.println("Max: " + max);

        amplist.add(new Float(relevantAmp));
        float average = Utility.averageList(amplist);
        System.out.println("Avg: " + average);
    }

    /**
     * Gets the average bandwith over a part of the spectrum
     * @param spectrum the spectrum array (usually 256 wide)
     * @param start where to start averaging [must be within spectrums length!]
     * @param end where to end averaging [must be within spectrums length!]
     * @return the average
     */
    public float getAverageAmp(float[] spectrum, int start, int end){
        if(start > spectrum.length || end > spectrum.length){
            return 0;
        }
        float avg = 0;
        for(int i = start; i< end; i++){
            avg += spectrum[i];
        }
        int bandwidth = end - start;
        avg = avg / bandwidth;
        return avg;
    }


    public void Pointillize (int width, int height)  {
        // Pick a random point

        int x = (int) random(coverImage.width);
        int y = (int) random(coverImage.height);
        int loc = x + y*coverImage.width;
        // Look up the RGB color in the source image
        loadPixels();
        float r = red(coverImage.pixels[loc]);
        float g = green(coverImage.pixels[loc]);
        float b = blue(coverImage.pixels[loc]);
        noStroke();

        // Draw an ellipse at that location with that color
        fill(r,g,b,100);
        ellipse(x,y,width,height);
    }


    public void multiDilate (int factor, boolean dilate){
        int filter = dilate? PApplet.DILATE : PApplet.ERODE;
        for (int i = factor; i >0; i--){
            filter(filter);
        }
    }

    /**
     * repeadetly calls the "invert" filter
     * @param factor how often to call the filter
     */
    public void invertImage (int factor){
        int filter = PApplet.INVERT;
        filter(filter);
    }

    /**
     * repeadetly calls the "posterize" filter
     * @param factor how often to call the filter
     */
    public void posterizeImage (int factor){
        int factor2 = factor;
        if(factor2 < 2){
            factor2 = 2;
        }
        if(factor2> 250){
            factor2 = 250;
        }
        int filter = PApplet.POSTERIZE;
            filter(filter, factor2);
            filter(filter, factor2);

    }

    /**
     * Produces a wave effect if continuously called
     * distortion should be around 5
     * wavespeed smaller than 0.3
     * tightness around 1
     */
    public void waveyDistort(float distortion, float wavespeed, float tightness){

        for (int i = 0; i < 640; ++i) {
            copy(i,0,1,height, i, (int) (sin((millis()+i*tightness)*wavespeed)*distortion),1,height);
        }
    }

    /**
     * repeadetly calls the "posterize" filter
     * @param factor how often to call the filter
     */
    public void grayImage (int factor){
        int filter = PApplet.GRAY;
            filter(filter);

    }
    /**
     * repeadetly calls the "invert" filter
     * @param factor how often to call the filter
     */
    public void blurImage (int factor){
        int filter = PApplet.BLUR;
        for (int i = factor; i >0; i--){
            filter(filter);
        }
    }
    /**
     * Start-Pause Funktion
     */
    public void mouseClicked() {
        if (mouseButton == LEFT) {
            noLoop();
            track.pause();
        } else if (mouseButton == RIGHT) {
            loop();
            track.play();
        } else if (mouseButton == CENTER) {
            background(0);
        }
    }

    /**
     * Malt das originale Coverimage
     */
    public void drawCoverImage(){
        image(coverImage,0,0);
    }

    /**
     * draw the cover image with some transparency
     * @param transparency from 0 (no transparency) to 255 invisible
     */
    public void drawTransparentCoverImage(int transparency){
        tint(255, transparency);
        drawCoverImage();
        tint(255);
    }

    /**
     * Generates the current spectrum after skipping one step forward
     * @return a float array of the spectrum 256 wide
     */
    public float[] getSpectrum(){
        fft.forward(track.mix);

        float[] spectrum = new float[bands];// 0-256;

        for(int i = 0; i < fft.avgSize(); i++)
        {
            spectrum[i] = fft.getAvg(i) / 2;

            // Smooth the FFT spectrum data by smoothing factor
            sum[i] += (abs(spectrum[i]) - sum[i]) * smoothingFactor;
        }

        return spectrum;
    };
    public void drawEqualizer(float[] spectrum, boolean randomColor){
        //rect(0, 0, width, height);



        for (int i = 0; i < spectrum.length; i++) {
            if(randomColor){
                fill(getRandomColor());
            }
            float value = spectrum[i];
            // accessing each element of array

            rect(0, i*2,value*20,5);
            if(i < 100){
                if(value > 0.05){

                }
            }
        }
        rect(0, 100,300,5); //i = 50
        rect(0, 200,300,5); // i = 100
        rect(0, 400,300,5); //i = 200
        rect(0, 400,300,5); //i = 200

    }

    /**
     * returns a random color as int (which is how processing colors are handled)
     */
    int getRandomColor(){
        int red = (int)  random(50, 255);
        int green = (int)  random(50, 255);
        int blue = (int)  random(50, 255);
        return color(red, green,blue);
    }



}