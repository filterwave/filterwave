 import com.wrapper.spotify.SpotifyApi;
        import com.wrapper.spotify.exceptions.SpotifyWebApiException;
        import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
 import com.wrapper.spotify.model_objects.specification.Paging;
 import com.wrapper.spotify.model_objects.specification.Track;
 import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
 import com.wrapper.spotify.requests.data.search.simplified.SearchTracksRequest;
 import org.apache.hc.core5.http.ParseException;

        import java.io.IOException;
        import java.util.concurrent.CancellationException;
        import java.util.concurrent.CompletableFuture;
        import java.util.concurrent.CompletionException;

public class ClientCredentialsExample {
    private static final String clientId = "665b0f6687a8421888be7e648e2035c8";
    private static final String clientSecret = "721f84e76a5d4284b8ca3366059944e0";

    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setClientId(clientId)
            .setClientSecret(clientSecret)
            .build();
    private static final ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials()
            .build();

    public static void clientCredentials_Sync() {
        try {
            final ClientCredentials clientCredentials = clientCredentialsRequest.execute();

            // Set access token for further "spotifyApi" object usage
            spotifyApi.setAccessToken(clientCredentials.getAccessToken());

        } catch (IOException | SpotifyWebApiException | ParseException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void clientCredentials_Async() {
        try {
            final CompletableFuture<ClientCredentials> clientCredentialsFuture = clientCredentialsRequest.executeAsync();

            // Thread free to do other tasks...

            // Example Only. Never block in production code.
            final ClientCredentials clientCredentials = clientCredentialsFuture.join();

            // Set access token for further "spotifyApi" object usage
            spotifyApi.setAccessToken(clientCredentials.getAccessToken());

        } catch (CompletionException e) {
            System.out.println("Error: " + e.getCause().getMessage());
        } catch (CancellationException e) {
            System.out.println("Async operation cancelled.");
        }
    }

    public static void main(String[] args) {
        clientCredentials_Sync();
        Track track = searchSingleTrack("Skepta");
        int i = 3;

    }

    public static Track searchSingleTrackAndAuthorize(String keywords){
        clientCredentials_Sync();
        return searchSingleTrack(keywords);
    }

    /**
     *  Searches for a single track and gives the URI of the FIRST of the possible results
     *  //todo maybe make this more precise / lenient (for stuff like "(Radio edit)"
     * @param artist the SINGULAR artist of the track [since spotifys Search sucks]
     * @return the spotify URI of the track
     *
     * NEVER search for multiple artists (like "Skepta feat. JME") since Spotifys search function cant handle that at all
     * (that's why RegExFileMan. has the "getFirstSingleArtist" function
     */
    public static Track searchSingleTrack(String artist) {

        SearchTracksRequest spotifyRequest =  spotifyApi.searchTracks(artist)
                .build();
        try {
            // Execute the request synchronous
            final Paging<Track> something = spotifyRequest.execute();
            Track track1 = null;
            try{
                track1 = something.getItems()[0];
            }catch(ArrayIndexOutOfBoundsException e){
                System.out.println("Mp3: "+ artist+" was NOT found on spotify");
                return null;
            }

            System.out.println("Mp3: "+ artist+", WAS found on spotify");
            return track1;
        } catch (Exception e) { //these exceptions are for the request itself
            System.out.println("Something went wrong!\n" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}

