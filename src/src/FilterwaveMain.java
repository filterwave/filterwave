import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import com.wrapper.spotify.model_objects.specification.Track;
import processing.core.PImage;

public class FilterwaveMain {

    /**
     * Main Entrypoint, asks for an artist, keyword etc and optionally a songname and then directly starts
     * @param args none
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String artistKeyword;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter your artist, keyword, genre etc.");
        artistKeyword = reader.readLine();

        PImage pimage = accessSpotifyWithSongInfo(artistKeyword);
        Processing_Entrypoint.startProcessing(artistKeyword, pimage);
    }


    /**
     * Does everything to do with Spotify.
     * Prints a link which HAS to be clicked to log into spotify!
     * Also downloads the 30 second preview to "filterwave30secPreview.mp3";
     * @param artistOrKeyword (cannot be null)
     * @return PImage, the cover image of the song found
     * @throws IOException
     *
     * ILLEGAL SONGS / KEYWORDS:
     * dua lipa - levitating
     * tame impala
     *Cage the elephant
     * The Flood - Take That
     * Move bitch - ludacris
     * Bloodhound Gang
     */
    public static PImage accessSpotifyWithSongInfo(String artistOrKeyword) throws IOException{
       // SpotifyHandler apiPoint = new SpotifyHandler();
       // ResponseCatchWebServer server = new ResponseCatchWebServer(apiPoint);
       // System.out.println(apiPoint.loginLink());
       // apiPoint.waitForLogin();
      //  server.stop();
        Track trackInfo = ClientCredentialsExample.searchSingleTrackAndAuthorize(artistOrKeyword);

        String previewUrl = trackInfo.getPreviewUrl();
        getPreviewMp3(previewUrl);

        String coverUrl = trackInfo.getAlbum().getImages()[0].getUrl();
        Image image = getImage(coverUrl);
        PImage pimage = new PImage(image);

        return pimage;
    }
    /**
     * A Method to download an image from the internet
     * @param urlString the URL to the image, can be gotten from a spotify track via trackInfo.getAlbum().getImages()[0].getUrl();
     * @return an Image instance (using the native awt framework)
     *
     * sample image URL (from spotify) https://i.scdn.co/image/ab67616d0000b27308cf5e81eded653e46299626
     */
    public static Image getImage(String urlString){
        Image image = null;
        try {
            URL url = new URL(urlString);
            image = ImageIO.read(url);
        } catch (IOException e) {
            System.out.println("Getting the Image failed");
            e.printStackTrace();
            System.exit(3);
        }
        return image;
    }

    /**
     * A method to download the 30 second preview URL from Spotify
     * @param previewUrl the path to the mp3, see sample
     * @return the path to the downloaded file
     * sample previewURL: https://p.scdn.co/mp3-preview/4288fc1c522953195ea5a56d8c03bcbf650c9d22?cid=665b0f6687a8421888be7e648e2035c8
     */
    public static String getPreviewMp3(String previewUrl){
        String outputFile = "filterwave30secPreview.mp3";
        try{
            InputStream in = new URL(previewUrl).openStream();
            Files.copy(in, Paths.get(outputFile), StandardCopyOption.REPLACE_EXISTING);
        }catch(IOException e){
            System.out.println("Failed downloading preview file");
            e.printStackTrace();
            System.exit(2);
        }
        return outputFile;
    }
}
